package sn.solution.paysen.rest;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;
import okhttp3.Call;
import okhttp3.FormBody;
import okhttp3.HttpUrl;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import org.restlet.resource.Post;
import org.restlet.resource.ServerResource;
import com.google.gson.Gson;
import sn.solution.paysen.entities.TransactionModel;
import sn.solution.paysen.entities.AmountObject;
import sn.solution.paysen.entities.RequestPMRestlet;
import sn.solution.paysen.entities.ResponseGetToken;
import sn.solution.paysen.entities.ResponsePMRestlet;


public class InitiationTXPMService extends ServerResource{

	private Logger LOGGER = Logger.getLogger(this.getClass().getName());

	TransactionModel transactionModel;
	Gson gson = new Gson();


	@SuppressWarnings("null")
	@Post("json")
	public String initierTX (String transactionExchange)
	{
		transactionModel = gson.fromJson(transactionExchange, TransactionModel.class);
		LOGGER.info(gson.toJson(transactionModel));
		
		//Recuperation du token
		ResponseGetToken responseGetToken = new ResponseGetToken();
		if (transactionModel.getAccess_tokenOM() == null)
		{
			responseGetToken = getToken(transactionModel.getUrl_getToken(), transactionModel.getClient_id(), transactionModel.getClient_secret(), transactionModel.getGrant_type());
		}
		else
		{
			LOGGER.info("Access Token");
			LOGGER.info(gson.toJson(transactionModel.getAccess_tokenOM()));
			responseGetToken.setAccess_token(transactionModel.getAccess_tokenOM());
			responseGetToken.setErrorCode("200");
			responseGetToken.setErrorMessage("Token interne bien reçu");

		}

		transactionModel.setAccess_tokenOM(responseGetToken.getAccess_token());
		transactionModel.setErrorCode(responseGetToken.getErrorCode());
		transactionModel.setErrorMessage(responseGetToken.getErrorMessage());
		
		//Recuperation de la clé public
		if(responseGetToken.getAccess_token() != null && !responseGetToken.getAccess_token().isEmpty())
		{
			//Initiation de la transaction
				ResponsePMRestlet responsePMRestlet = initTransaction(responseGetToken.getAccess_token(), transactionModel);
			
				transactionModel.setErrorCode(responsePMRestlet.getCode());
				transactionModel.setErrorMessage(responsePMRestlet.getErrorMessage());
				
				if (responsePMRestlet.getErrorCode().equalsIgnoreCase("201"))
				{
					transactionModel.setErrorCode(responsePMRestlet.getErrorCode());
					transactionModel.setErrorMessage(responsePMRestlet.getErrorMessage());
					transactionModel.setDeepLinks(responsePMRestlet.getDeepLinks());
					transactionModel.setQrCode(responsePMRestlet.getQrCode());
					transactionModel.setMetadata(responsePMRestlet.getMetadata());
					transactionModel.setValidFor(responsePMRestlet.getValidFor());
				}
		}
		
		
		return gson.toJson(transactionModel);
		
	}

	//Recupération TOKEN
	public ResponseGetToken getToken (String url, String client_id, String client_secret, String grant_type)
	{
		Gson gson = new Gson();
		ResponseGetToken responseGetToken = new ResponseGetToken();
		
		try {

			LOGGER.info("Request getToken OM ... ");
		
			HttpUrl.Builder httpBuilder = HttpUrl.parse(url).newBuilder();
	
						
			OkHttpClient client = new OkHttpClient();
			
			RequestBody formBody = new FormBody.Builder()
				      .add("client_id", client_id)
				      .add("client_secret", client_secret)
				      .add("grant_type",grant_type)

				      
				      .build();
			
			Request request = new Request.Builder()
				      .url(httpBuilder.build())
				      .post(formBody)
				      .build();

			
			Call call = client.newCall(request);
		    Response response = call.execute();
			
//execute is a synchronous GET request, you can directly get the Response object
		   
            if(response.isSuccessful()){
                String repbody=response.body().string();
               
                	responseGetToken.setErrorCode("200");
                	responseGetToken.setErrorMessage("Token bien reçu ...");
                
                LOGGER.info("Response getToken : "+repbody);
                
                responseGetToken = gson.fromJson(repbody, ResponseGetToken.class);

            }
            else
            {
                String repbody=response.body().string();

                responseGetToken.setErrorCode("201");
                responseGetToken.setErrorMessage("Le token n'a pas été reçu, merci de vérifier les paramètres ...");

				LOGGER.info("Not success response : "+repbody);
            }
		
				} 
				catch(Exception e) {
					e.printStackTrace();
					
					responseGetToken.setErrorCode("201");
					responseGetToken.setErrorMessage("Le token n'a pas été reçu, merci de vérifier les paramètres ...");
			
				}
		return responseGetToken;
	}

	
	public ResponsePMRestlet initTransaction (String token, TransactionModel transactionModel)
	{
		RequestPMRestlet requestPMRestlet = null;
		ResponsePMRestlet responsePMRestlet  = null;
		AmountObject amount = null;
		
		try {
			
			
			Gson gson = new Gson();
			requestPMRestlet = new RequestPMRestlet();
			responsePMRestlet = new ResponsePMRestlet();
			
			amount = new AmountObject();
			Map<String, String> metadata = new HashMap<String, String>();

			
			//Recuperation amount
			amount.setValue(transactionModel.getMontant());
			amount.setUnit(transactionModel.getUnit_amount());
			
			//Mise en place request
			requestPMRestlet.setAmount(amount);
			requestPMRestlet.setCallbackSuccessUrl(transactionModel.getCallbackSuccessUrl());
			requestPMRestlet.setCallbackCancelUrl(transactionModel.getCallbackCancelUrl());
			requestPMRestlet.setCode(transactionModel.getCode());
			requestPMRestlet.setName(transactionModel.getName());
			requestPMRestlet.setValidity(transactionModel.getValidity());
			metadata.put("tokenTX", transactionModel.getIdentifiantTXPaysen());
			requestPMRestlet.setMetadata(metadata);
			


			LOGGER.info("Request PM : "+ gson.toJson(requestPMRestlet));
			
			HttpUrl.Builder httpBuilder = HttpUrl.parse(transactionModel.getUrl_payment()).newBuilder();

						
			OkHttpClient client = new OkHttpClient();

			MediaType mediaType = MediaType.parse("application/json");
			String data = gson.toJson(requestPMRestlet);
			
			RequestBody body = RequestBody.create(mediaType, data);
			
			Request request = new Request.Builder().addHeader("Accept", "application/json")
			.addHeader("Authorization", "Bearer " + token)
			.addHeader("X-Callback-Url", transactionModel.getCallbackEndTx())
			.url(httpBuilder.build()).post(body).build();

			Response response = client.newCall(request).execute();
            if(response.isSuccessful()){
                String repbody=response.body().string();
               
                responsePMRestlet = gson.fromJson(repbody, ResponsePMRestlet.class);
                
                LOGGER.info("Response PM : "+gson.toJson(responsePMRestlet));
                
                responsePMRestlet.setErrorCode("201");
                responsePMRestlet.setErrorMessage("Paiement en cours. Merci de rediriger le client vers le lien de paiement");	
            }
            else
            {
                String repbody=response.body().string();

                responsePMRestlet = gson.fromJson(repbody, ResponsePMRestlet.class);

				//responsePMRestlet.setStatus("FAILED");
				LOGGER.info("Not success response");
				LOGGER.info(repbody);

				
				responsePMRestlet.setErrorCode("201");
				responsePMRestlet.setErrorMessage("La transaction est mise en échec ...");
                 if (responsePMRestlet.getDetail() != null)
                	 responsePMRestlet.setErrorMessage(responsePMRestlet.getDetail());
            }
		
				} 
				catch(Exception e) {
					e.printStackTrace();
					//LOGGER.info("Response code: " + e);
					
					responsePMRestlet.setStatus("FAILED");		
					responsePMRestlet.setErrorCode("201");
					responsePMRestlet.setErrorMessage("La transaction est mise en échec ...");
				}
		LOGGER.info("Response PM OM : ");
		return responsePMRestlet;

	}
}
