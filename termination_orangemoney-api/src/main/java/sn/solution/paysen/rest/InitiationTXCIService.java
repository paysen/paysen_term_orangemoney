package sn.solution.paysen.rest;

import java.nio.charset.StandardCharsets;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.interfaces.ECPublicKey;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import javax.crypto.Cipher;

import okhttp3.Call;
import okhttp3.FormBody;
import okhttp3.HttpUrl;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import org.restlet.resource.Post;
import org.restlet.resource.ServerResource;
import com.google.gson.Gson;
import sn.solution.paysen.entities.TransactionModel;
import sn.solution.paysen.entities.AmountObject;
import sn.solution.paysen.entities.CustomerObject;
import sn.solution.paysen.entities.PartnerObject;
import sn.solution.paysen.entities.RequestCIRestlet;
import sn.solution.paysen.entities.ResponseCIRestlet;
import sn.solution.paysen.entities.ResponseGetPublicKey;
import sn.solution.paysen.entities.ResponseGetToken;


public class InitiationTXCIService extends ServerResource{

	private Logger LOGGER = Logger.getLogger(this.getClass().getName());

	TransactionModel transactionModel;
	Gson gson = new Gson();


	@SuppressWarnings("null")
	@Post("json")
	public String initierTX (String transactionExchange)
	{
		transactionModel = gson.fromJson(transactionExchange, TransactionModel.class);
		LOGGER.info("Request init tx to OM");
		LOGGER.info(gson.toJson(transactionModel));
		
		//Recuperation du token
		ResponseGetToken responseGetToken = new ResponseGetToken();
		if (transactionModel.getAccess_tokenOM() == null)
		{
			responseGetToken = getToken(transactionModel.getUrl_getToken(), transactionModel.getClient_id(), transactionModel.getClient_secret(), transactionModel.getGrant_type());
		}
		else
		{
			LOGGER.info("Token interne bien reçu");

			LOGGER.info("Access Token");
			LOGGER.info(gson.toJson(transactionModel.getAccess_tokenOM()));
			responseGetToken.setAccess_token(transactionModel.getAccess_tokenOM());
			responseGetToken.setErrorCode("200");
			responseGetToken.setErrorMessage("Token interne bien reçu");

		}

		transactionModel.setAccess_tokenOM(responseGetToken.getAccess_token());
		transactionModel.setErrorCode(responseGetToken.getErrorCode());
		transactionModel.setErrorMessage(responseGetToken.getErrorMessage());
		
		//Recuperation de la clé public
		if(responseGetToken.getAccess_token() != null && !responseGetToken.getAccess_token().isEmpty())
		{
			String public_key = "";
			
			if (transactionModel.getPublic_key() != null)
			{
				public_key = transactionModel.getPublic_key();
			}
			else
			{
				ResponseGetPublicKey responseGetPublicKey = getPublic_key(transactionModel.getUrl_getPublicKey(), responseGetToken.getAccess_token());

				if (responseGetPublicKey != null)
				{
					public_key = responseGetPublicKey.getKey();
					
					transactionModel.setErrorCode(responseGetPublicKey.getErrorCode());
					transactionModel.setErrorMessage(responseGetPublicKey.getErrorMessage());
				}
			}

			
			//Initiation de la transaction
			if(public_key != null && !public_key.isEmpty())
			{
				ResponseCIRestlet responseCIRestlet = initTransaction(public_key, responseGetToken.getAccess_token(), transactionModel);
			
				transactionModel.setErrorCode(responseCIRestlet.getCode());
				transactionModel.setErrorMessage(responseCIRestlet.getErrorMessage());
				
				if (responseCIRestlet.getErrorCode().equalsIgnoreCase("200"))
				{
					transactionModel.setErrorCode("200");
					transactionModel.setErrorMessage("Dépôt effectué avec succès ...");
					transactionModel.setRefTransaction(responseCIRestlet.getTransactionId());
				}
			}
		}
		
		
		return gson.toJson(transactionModel);
		
	}

	//Recupération TOKEN
	public ResponseGetToken getToken (String url, String client_id, String client_secret, String grant_type)
	{
		Gson gson = new Gson();
		ResponseGetToken responseGetToken = new ResponseGetToken();
		
		try {

			LOGGER.info("Request getToken OM ... ");
		
			HttpUrl.Builder httpBuilder = HttpUrl.parse(url).newBuilder();
	
						
			OkHttpClient client = new OkHttpClient();
			
			RequestBody formBody = new FormBody.Builder()
				      .add("client_id", client_id)
				      .add("client_secret", client_secret)
				      .add("grant_type",grant_type)

				      
				      .build();
			
			Request request = new Request.Builder()
				      .url(httpBuilder.build())
				      .post(formBody)
				      .build();

			
			Call call = client.newCall(request);
		    Response response = call.execute();
			
//execute is a synchronous GET request, you can directly get the Response object
		   
            if(response.isSuccessful()){
                String repbody=response.body().string();
               
                	responseGetToken.setErrorCode("200");
                	responseGetToken.setErrorMessage("Token bien reçu ...");
                
                LOGGER.info("Response getToken : "+repbody);
                
                responseGetToken = gson.fromJson(repbody, ResponseGetToken.class);

            }
            else
            {
                String repbody=response.body().string();

                responseGetToken.setErrorCode("201");
                responseGetToken.setErrorMessage("Le token n'a pas été reçu, merci de vérifier les paramètres ...");

				LOGGER.info("Not success response : "+repbody);
            }
		
				} 
				catch(Exception e) {
					e.printStackTrace();
					
					responseGetToken.setErrorCode("201");
					responseGetToken.setErrorMessage("Le token n'a pas été reçu, merci de vérifier les paramètres ...");
			
				}
		return responseGetToken;
	}

	//Recupération Public Key
	public ResponseGetPublicKey getPublic_key (String url, String token)
	{
		Gson gson = new Gson();
		ResponseGetPublicKey responseGetPublicKey = new ResponseGetPublicKey();


		try {
			
			LOGGER.info(gson.toJson("Token to get Public Key OM : "+token));

			HttpUrl.Builder httpBuilder = HttpUrl.parse(url).newBuilder();
		
			Request request = new Request.Builder().addHeader("Accept", "application/json")
			.addHeader("Authorization", "Bearer " + token)
			.url(httpBuilder.build()).get().build();
			
			OkHttpClient client = new OkHttpClient();
			
			Call call=client.newCall(request);      //Create a Call
            Response response=call.execute();    //execute is a synchronous GET request, you can directly get the Response object
            if(response.isSuccessful()){
                String repbody=response.body().string();
               
                responseGetPublicKey = gson.fromJson(repbody, ResponseGetPublicKey.class);
				
				LOGGER.info(gson.toJson("Response Get Public Key OM : "+gson.toJson(responseGetPublicKey)));
				
				responseGetPublicKey.setErrorCode("200");
				responseGetPublicKey.setErrorMessage("Public key bien reçu ... ");
				
            }else{
				LOGGER.info("Echec de la recupération du Public key ");
				
				try {
					responseGetPublicKey = gson.fromJson(response.body().string(), ResponseGetPublicKey.class);
					LOGGER.info(gson.toJson(responseGetPublicKey));
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
				
				responseGetPublicKey.setErrorCode("201");
				responseGetPublicKey.setErrorMessage("Public key non reçu ... ");
				
            }
		
				} 
				catch(Exception e) {
					e.printStackTrace();
					LOGGER.info("Response code: " + e);
					
					responseGetPublicKey.setErrorCode("201");
					responseGetPublicKey.setErrorMessage("Public key non reçu ... ");
				}
		
		return responseGetPublicKey;
	}
	
	//Recupération Public Key
	public ResponseCIRestlet initTransaction (String public_keyString, String token, TransactionModel transactionModel)
	{
		
		//String pin = "2021";
		String pin = transactionModel.getPin();
		String encodePinWithPublicKey = "";
		
		try {
			
			//converting string to Bytes
			//byte[] byte_pubkey = public_keyString.getBytes();
			byte[] byte_pubkey = Base64.getDecoder().decode(public_keyString);

			//byte[] byte_pubkey = Base64.decode(public_keyString, Base64.NO_WRAP);
			System.out.println("BYTE KEY::" + byte_pubkey);

			//converting it back to public key
			KeyFactory factory = KeyFactory.getInstance("RSA");
			PublicKey public_key = factory.generatePublic(new X509EncodedKeySpec(byte_pubkey));
			System.out.println("FINAL OUTPUT" + public_key);

			//Encryptage PIN with public key
			Cipher encryptCipher = Cipher.getInstance("RSA");
			encryptCipher.init(Cipher.ENCRYPT_MODE, public_key);
			byte[] secretMessageBytes = pin.getBytes(StandardCharsets.UTF_8);
			byte[] encryptedMessageBytes = encryptCipher.doFinal(secretMessageBytes);
			
			encodePinWithPublicKey = Base64.getEncoder().encodeToString(encryptedMessageBytes);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}


		RequestCIRestlet requestCIRestlet = null;
		ResponseCIRestlet responseCIRestlet  = null;
		PartnerObject partner = null;
		CustomerObject customer = null;
		AmountObject amount = null;
		
		try {
			
			
			Gson gson = new Gson();
			requestCIRestlet = new RequestCIRestlet();
			responseCIRestlet = new ResponseCIRestlet();
			
			partner = new PartnerObject();
			customer = new CustomerObject();
			amount = new AmountObject();
			

					
			//Recuperation partner 
			partner.setId(transactionModel.getNumeroDistributeur());
			partner.setIdType(transactionModel.getMsisdn());
			partner.setEncryptedPinCode(encodePinWithPublicKey);
			
			//Recuperation customer
			customer.setId(transactionModel.getNumeroBeneficiaire());
			customer.setIdType(transactionModel.getMsisdn());
			
			//Recuperation amount
			amount.setValue(transactionModel.getMontant());
			amount.setUnit(transactionModel.getUnit_amount());
			
			//Mise en place request
			requestCIRestlet.setPartner(partner);
			requestCIRestlet.setCustomer(customer);
			requestCIRestlet.setAmount(amount);
			requestCIRestlet.setReference(transactionModel.getIdentifiantTXPaysen());
			requestCIRestlet.setReceiveNotification(true);
			


			LOGGER.info("Request CASHINOM : "+ gson.toJson(requestCIRestlet));
			
			HttpUrl.Builder httpBuilder = HttpUrl.parse(transactionModel.getUrl_cashin()).newBuilder();

						
			OkHttpClient client = new OkHttpClient();

			MediaType mediaType = MediaType.parse("application/json");
			String data = gson.toJson(requestCIRestlet);
			
			RequestBody body = RequestBody.create(mediaType, data);
			
			Request request = new Request.Builder().addHeader("Accept", "application/json")
//			.addHeader("Content-Type", "application/json")
			.addHeader("Authorization", "Bearer " + token)
			.url(httpBuilder.build()).post(body).build();

			Response response = client.newCall(request).execute();
//execute is a synchronous GET request, you can directly get the Response object
            if(response.isSuccessful()){
                String repbody=response.body().string();
               
                responseCIRestlet = gson.fromJson(repbody, ResponseCIRestlet.class);
                
                LOGGER.info("Response CASHINOM : "+gson.toJson(responseCIRestlet));
                
                if (responseCIRestlet.getStatus().equalsIgnoreCase("SUCCESS"))
                {
                    responseCIRestlet.setErrorCode("200");
                    responseCIRestlet.setErrorMessage("Le dépôt est effectué avec succès ...");
                }
                else
                {
                    responseCIRestlet.setErrorCode("201");
                	responseCIRestlet.setErrorMessage("La transaction est mise en échec ...");

                    if (responseCIRestlet.getDetail() != null)
                    	responseCIRestlet.setErrorMessage(responseCIRestlet.getDetail());
                }

				
            }
            else
            {
                String repbody=response.body().string();

                responseCIRestlet = gson.fromJson(repbody, ResponseCIRestlet.class);

				//responsePMRestlet.setStatus("FAILED");
				LOGGER.info("Not success response");
				LOGGER.info(repbody);

				
				 responseCIRestlet.setErrorCode("201");
                 responseCIRestlet.setErrorMessage("La transaction est mise en échec ...");
                 if (responseCIRestlet.getDetail() != null)
                 	responseCIRestlet.setErrorMessage(responseCIRestlet.getDetail());
            }
		
				} 
				catch(Exception e) {
					e.printStackTrace();
					//LOGGER.info("Response code: " + e);
					
					responseCIRestlet.setStatus("FAILED");		
					responseCIRestlet.setErrorCode("201");
	                responseCIRestlet.setErrorMessage("La transaction est mise en échec ...");
				}
		LOGGER.info("Response CASHINOM : ");
		return responseCIRestlet;

	}
}
