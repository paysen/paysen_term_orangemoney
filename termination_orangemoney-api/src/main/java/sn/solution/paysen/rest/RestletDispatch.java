package sn.solution.paysen.rest;
import java.util.Arrays;
import java.util.HashSet;

import org.restlet.Application;
import org.restlet.Restlet;
import org.restlet.engine.Engine;
import org.restlet.ext.crypto.DigestAuthenticator;
import org.restlet.ext.gson.GsonConverter;
import org.restlet.routing.Router;
import org.restlet.routing.Template;
import org.restlet.security.LocalVerifier;
import org.restlet.security.MapVerifier;
import org.restlet.service.CorsService;


public class RestletDispatch extends Application  {

	private LocalVerifier verifier = null;

	public RestletDispatch() {
		MapVerifier mapVerifier = new MapVerifier();
		//mapVerifier.getLocalSecrets().put("v7PzCajBd1QQa28AzswPD4OIrKX21MORVaY7S4DnGOeEfpFTJ3", "ri520S5XqKySB93Ceg3H8TIwqE4uzp".toCharArray());
		mapVerifier.getLocalSecrets().put("QSzMTcVqSoGyGDgaaOF7J2DPtsoeftVDLz8bCZ3f63OfVuY0cz", "vqV58rqOnKHWhOtUyI7xPW9KQ7CGoFeDI5J".toCharArray());

		this.verifier = mapVerifier;
		CorsService corsService = new CorsService();
		corsService.setAllowedOrigins(new HashSet(Arrays.asList("*")));
		corsService.setAllowedCredentials(true);
		corsService.setAllowingAllRequestedHeaders(true);
		corsService.setSkippingResourceForCorsOptions(true);
		getServices().add(corsService);
	}

	@Override
	public synchronized Restlet createInboundRoot() {

		//ParametreService  parametreService = (ParametreService) JNDIUtils.lookUpEJB(EJBRegistry.ParametreServiceBean);

		Router router = new Router(getContext());

		DigestAuthenticator authenticator = new DigestAuthenticator(getContext(), "WfmMyX4ciR", "5wbPRPWBu6DJyiR16W8OaVPcIyq5fWJu");
		authenticator.setWrappedVerifier(this.verifier);
		router.setDefaultMatchingMode(Template.MODE_EQUALS) ;
		Engine.getInstance().getRegisteredConverters().add(new GsonConverter());
		//router.attach("/initiationTransactionPM", InitiationTransactionPMService.class);
		router.attach("/initiationTXCI", InitiationTXCIService.class);
		router.attach("/initiationTXCO", InitiationTXCOService.class);
		router.attach("/initiationTXPM", InitiationTXPMService.class);

		authenticator.setNext(router);
		return authenticator;
	}

}
