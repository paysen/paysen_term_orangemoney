package sn.solution.paysen.rest;

public class ServiceConstants {

		public static final String SN_CASHIN_OM = "SN_CASHIN_OM";
		public static final String SN_CASHOUT_OM = "SN_CASHOUT_OM";
		public static final String SN_PM_OM = "SN_PM_OM";
		public static final String SN_CASHIN_FREE_MONEY = "SN_CASHIN_FREE_MONEY";
		public static final String SN_CASHOUT_FREE_MONEY = "SN_CASHOUT_FREE_MONEY";
		public static final String SN_PM_FREE_MONEY = "SN_PM_FREE_MONEY";
		public static final String SN_CASHIN_EMONEY = "SN_CASHIN_EMONEY";
		public static final String SN_CASHOUT_EMONEY = "SN_CASHOUT_EMONEY";
		public static final String SN_PM_EMONEY = "SN_PM_EMONEY";
		public static final String SN_AIRTIME_ORANGE = "SN_AIRTIME_ORANGE";
		public static final String SN_AIRTIME_FREE = "SN_AIRTIME_FREE";
		public static final String SN_AIRTIME_EXPRESSO = "SN_AIRTIME_EXPRESSO";
		public static final String SN_AIRTIME_PROMOBILE = "SN_AIRTIME_PROMOBILE";
		public static final String BANK_PAYMENT = "BANK_PAYMENT";
		public static final String BANK_TRANSFER = "BANK_TRANSFER";
		public static final String PB_ECOBANK = "PB_ECOBANK";
		public static final String SN_PM_WIZALL = "SN_PM_WIZALL";
		public static final String SN_PM_WAVE = "SN_PM_WAVE";
		public static final String SN_CASHIN_WIZALL = "SN_CASHIN_WIZALL";
		public static final String SN_CASHOUT_WIZALL = "SN_CASHOUT_WIZALL";

		

}
