package sn.solution.paysen.rest;
import java.util.logging.Logger;
import okhttp3.HttpUrl;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import org.restlet.resource.Post;
import org.restlet.resource.ServerResource;
import com.google.gson.Gson;
import sn.solution.paysen.entities.RequestCORestlet;
import sn.solution.paysen.entities.ResponseCORestlet;


public class InitiationTXCOService extends ServerResource{

	private Logger LOGGER = Logger.getLogger(this.getClass().getName());


	@Post("json")
	public String initierTX (String transactionExchange)
	{
		Gson gson = new Gson();
		//TransactionModel transactionModel = gson.fromJson(transactionExchange, TransactionModel.class);
		ResponseCORestlet responseCORestlet = new ResponseCORestlet();
		RequestCORestlet requestCORestlet = gson.fromJson(transactionExchange, RequestCORestlet.class);
		
		try {
					
			
			//Objet constituant le corps
//			requestCORestlet.setUsername("221763051997");
//			requestCORestlet.setPassword("9090");
//			requestCORestlet.setAmount(transactionModel.getMontant());
			requestCORestlet.setCurrency("XOF");
//			requestCORestlet.setCustomermsisdn(null);
//			requestCORestlet.setExternalid(transactionModel.getIdentifiantTXPaysen());

			LOGGER.info("Request CASHOUT Free Money : "+gson.toJson(requestCORestlet));

			
			HttpUrl.Builder httpBuilder = HttpUrl.parse(requestCORestlet.getUrl_cashout()).newBuilder();
			
			requestCORestlet.setUrl_cashout(null);
			
//			String auth = "MTN" + ":"
//					+ "passer";
//					byte[] encodedAuth = Base64.encodeBase64(auth.getBytes(Charset.forName("UTF-8")));
//					String authHeader = "Basic " + new String(encodedAuth);
				    

					
					
						
			OkHttpClient client = new OkHttpClient();
			

			MediaType mediaType = MediaType.parse("application/json");
			String data = gson.toJson(requestCORestlet);
			
			RequestBody body = RequestBody.create(mediaType, data);
			
			Request request = new Request.Builder()
			.addHeader("Content-Type", "application/json")
			//.addHeader("Accept", "application/json")
			//.addHeader("Authorization", authHeader)
			.url(httpBuilder.build()).post(body).build();

			Response response = client.newCall(request).execute();
//execute is a synchronous GET request, you can directly get the Response object

            if(response.isSuccessful()){
                String repbody=response.body().string();
                //String repbody = response.peekBody(99999L).string();
               
                
                if (repbody != null && !repbody.isEmpty()) 
                	{
                    responseCORestlet.setStatus("PENDING");
                    responseCORestlet.setMessage("Le retrait est bien initié, merci de procéder à la validation par USSD ...");

                	responseCORestlet = gson.fromJson(repbody, ResponseCORestlet.class);
                	
	                	if (responseCORestlet.getRoot() != null)
	                	{
	                		  responseCORestlet.setStatus(responseCORestlet.getRoot().getStatus());
	                          responseCORestlet.setMessage(responseCORestlet.getRoot().getMessage());
	                	}
	                	else if (responseCORestlet.getStatus() != null && responseCORestlet.getMessage() != null)
	                	{
	                		  responseCORestlet.setStatus(responseCORestlet.getStatus());
	                          responseCORestlet.setMessage(responseCORestlet.getMessage());
	                	}
                	}
                else
                {
//                    responseCORestlet.setStatus("FAILED");
//                    responseCORestlet.setMessage("La transaction est mise en échec ...");
                    
                    try {
                        responseCORestlet = gson.fromJson(repbody, ResponseCORestlet.class);
    				} catch (Exception e) {
    					// TODO: handle exception
    					responseCORestlet.setStatus("FAILED");
    		             responseCORestlet.setMessage("La transaction est mise en échec ...");
    				}

                }
                
    			LOGGER.info("Response CASHOUT Free Money : "+repbody);
                //LOGGER.info("Response CASHOUT Free Money : "+gson.toJson(responseCORestlet));
                
   
				
            }
            else
            {
                String repbody=response.body().string();

                try {
                    responseCORestlet = gson.fromJson(repbody, ResponseCORestlet.class);
				} catch (Exception e) {
					// TODO: handle exception
					responseCORestlet.setStatus("FAILED");
		             responseCORestlet.setMessage("La transaction est mise en échec ...");
				}

				//responsePMRestlet.setStatus("FAILED");
				LOGGER.info("Not success response : "+ repbody);
				
				 

            }
		
				} 
				catch(Exception e) {
					e.printStackTrace();
					//LOGGER.info("Response code: " + e);
					
					responseCORestlet.setStatus("FAILED");
		            responseCORestlet.setMessage("Erreur système ...");

				}
		return gson.toJson(responseCORestlet);
	}
}
