package sn.solution.paysen.ws.rest;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

import com.google.gson.JsonObject;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class Main {
	private static Logger logger = Logger.getLogger(Main.class);

	public static void main(String[] args) {
		JsonObject jo = new JsonObject();
		jo.addProperty("serial", "10000450000057");
		final DigestAuthenticator authenticator = new DigestAuthenticator(
				new Credentials("$$$intouchTotalCrede%%%%%", "koala"));

		final Map<String, CachingAuthenticator> authCache = new ConcurrentHashMap<String, CachingAuthenticator>();


		final OkHttpClient client = new OkHttpClient.Builder().readTimeout(2L * 60, TimeUnit.SECONDS)
				.connectTimeout(30, TimeUnit.SECONDS)
				.authenticator(new CachingAuthenticatorDecorator(authenticator, authCache))
				.addInterceptor(new AuthenticationCacheInterceptor(authCache)).build();
		MediaType mediaType = MediaType.parse("application/json");
		RequestBody body = RequestBody.create(mediaType, jo.toString());
		Request request = new Request.Builder().url("http://localhost:8080/sdbclient/boncarb/check_bon").post(body)
				.build();

		try {
			Response response = client.newCall(request).execute();
			if (response != null) {
				String codeResponse = response.networkResponse().code() + "";
				String responseBody = response.body().string();
				System.out.println("TOUCH API RESPONSE CODE: " + codeResponse);
				System.out.println("TOUCH API RESPONSE BODY: " + responseBody);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
