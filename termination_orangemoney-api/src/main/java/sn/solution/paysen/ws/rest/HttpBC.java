package sn.solution.paysen.ws.rest;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.restlet.data.ChallengeRequest;
import org.restlet.data.ChallengeResponse;
import org.restlet.data.ChallengeScheme;
import org.restlet.data.MediaType;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.ClientResource;
import org.restlet.resource.ResourceException;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class HttpBC {
	private static Logger logger = Logger.getLogger(HttpBC.class);

	/*
	 * public static JsonObject sendNotification(JsonObject op) { JsonObject jo
	 * = null; CloseableHttpAsyncClient httpclient =
	 * HttpAsyncClients.createDefault(); String
	 * urlNotif="http//:192.168.2.45:8080/boncarb/check_bon"; try { // Start the
	 * client httpclient.start(); HttpPost request = new HttpPost(urlNotif);
	 * String auth = "$$$intouchTotalCrede%%%%%" + ":" + "koala"; byte[]
	 * encodedAuth =
	 * Base64.encodeBase64(auth.getBytes(Charset.forName("UTF-8"))); String
	 * authHeader = "Basic " + new String(encodedAuth);
	 * request.setHeader(HttpHeaders.AUTHORIZATION, authHeader);
	 * request.addHeader("Content-Type", "application/json");
	 * 
	 * Prepare StringEntity from JSON StringEntity jsonData = new
	 * StringEntity(op.toString(), "UTF-8");
	 * 
	 * Body of request request.setEntity(jsonData);
	 * 
	 * try { Future<HttpResponse> future = httpclient.execute(request, null);
	 * HttpResponse response = future.get();
	 * 
	 * if (response.getStatusLine().getStatusCode() == 200) { InputStream
	 * source; source = (response).getEntity().getContent(); // Get the data in
	 * the entity Reader reader = new InputStreamReader(source, "UTF-8"); Gson g
	 * = new Gson(); jo=g.fromJson(reader, JsonObject.class);
	 * 
	 * } else { request.abort(); } } catch (Exception e) { e.printStackTrace();
	 * } } catch (Exception exception) { exception.printStackTrace(); } finally
	 * { try { httpclient.close();
	 * 
	 * } catch (Exception e) { e.printStackTrace(); } } return jo; }
	 */
	public static JsonObject sendPostRequest(String url, JsonObject jo) {
		JsonObject joRep = null;
		final DigestAuthenticator authenticator = new DigestAuthenticator(
				new Credentials("v7PzCajBd1QQa28AzswPD4OIrKX21MORVaY7S4DnGOeEfpFTJ3", "ri520S5XqKySB93Ceg3H8TIwqE4uzp"));

		final Map<String, CachingAuthenticator> authCache = new ConcurrentHashMap<String, CachingAuthenticator>();

		final OkHttpClient client = new OkHttpClient.Builder().readTimeout(2L* 60, TimeUnit.SECONDS)
				.connectTimeout(30, TimeUnit.SECONDS)
				.authenticator(new CachingAuthenticatorDecorator(authenticator, authCache))
				.addInterceptor(new AuthenticationCacheInterceptor(authCache)).build();
		okhttp3.MediaType mediaType = okhttp3.MediaType.parse("application/json");
		RequestBody body = RequestBody.create(mediaType, jo.toString());
		Request request = new Request.Builder().url(url).post(body).build();

		try {
			Response response = client.newCall(request).execute();
			if (response != null) {
				String codeResponse = response.networkResponse().code() + "";
				String responseBody = response.body().string();
				logger.info("BILLETTERIE API RESPONSE CODE: " + codeResponse);
				logger.info("BILLETTERIE API RESPONSE BODY: " + responseBody);
				if (response.code() == 200) {
					joRep = getJson(responseBody);
				}
			}
		} catch (IOException e) {
			logger.error("", e);
		}
		return joRep;
	}

	public static JsonObject sendPostRequest1(String url, JsonObject jo) {
		try {
			StringRepresentation stringRep = new StringRepresentation(jo.toString());
			stringRep.setMediaType(MediaType.APPLICATION_JSON);

			ClientResource resource = new ClientResource(url);

			resource.setChallengeResponse(ChallengeScheme.HTTP_DIGEST, "$$$4FDGintouchFVDjknfgflezreyTotal243576Crede%%%%%", "ko5FDFGHtrtyyuaTTEFoury3237980la");
			// Send the first request with unsufficient authentication.
			try {
				resource.post(stringRep);
			} catch (ResourceException re) {
			}
			// Should be 401, since the client needs some data sent by the
			// server in
			// order to complete the ChallengeResponse.
			logger.info(resource.getStatus());

			// Complete the challengeResponse object according to the server's
			// data
			// 1- Loop over the challengeRequest objects sent by the server.
			ChallengeRequest c1 = null;
			for (ChallengeRequest challengeRequest : resource.getChallengeRequests()) {
				if (ChallengeScheme.HTTP_DIGEST.equals(challengeRequest.getScheme())) {
					c1 = challengeRequest;
					break;
				}
			}

			// 2- Create the Challenge response used by the client to
			// authenticate
			// its requests.
			ChallengeResponse challengeResponse = new ChallengeResponse(c1, resource.getResponse(),
					"$$$4FDGintouchFVDjknfgflezreyTotal243576Crede%%%%%", "ko5FDFGHtrtyyuaTTEFoury3237980la".toCharArray());
			resource.setChallengeResponse(challengeResponse);
			StringRepresentation stringRep2 = new StringRepresentation(jo.toString());
			stringRep2.setMediaType(MediaType.APPLICATION_JSON);

			// Try authenticated request
			resource.post(stringRep2);
			// Should be 200.
			logger.info(resource.getStatus());
			logger.info(resource.getResponseEntity());
			String output = resource.getResponse().getEntityAsText();
			logger.info("Output from Server .... \n");
			logger.info(output);

			if (resource.getStatus().getCode() == 200) {
				return getJson(output);
			} else {
				return null;
			}
		} catch (Exception e) {
			return null;
		}

	}

	private static JsonObject getJson(String str) {
		JsonParser jp = new JsonParser();
		JsonObject jo = jp.parse(str).getAsJsonObject();

		return jo;
	}

}
