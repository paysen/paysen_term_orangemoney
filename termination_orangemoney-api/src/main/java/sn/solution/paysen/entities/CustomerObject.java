package sn.solution.paysen.entities;

import java.io.Serializable;

public class CustomerObject implements Serializable{

	private String idType;
	private String id;
	
	public String getIdType() {
		return idType;
	}
	public void setIdType(String idType) {
		this.idType = idType;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	
}
