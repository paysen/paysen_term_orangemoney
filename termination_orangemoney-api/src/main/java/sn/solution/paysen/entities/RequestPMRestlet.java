package sn.solution.paysen.entities;

import java.io.Serializable;
import java.util.Map;

public class RequestPMRestlet implements Serializable{

	private AmountObject amount;
	private String callbackCancelUrl;
	private String callbackSuccessUrl;
	private String code;
	private Map<String, String> metadata;
	private String name;
	private Long validity;
	
	public AmountObject getAmount() {
		return amount;
	}
	public void setAmount(AmountObject amount) {
		this.amount = amount;
	}
	public String getCallbackCancelUrl() {
		return callbackCancelUrl;
	}
	public void setCallbackCancelUrl(String callbackCancelUrl) {
		this.callbackCancelUrl = callbackCancelUrl;
	}
	public String getCallbackSuccessUrl() {
		return callbackSuccessUrl;
	}
	public void setCallbackSuccessUrl(String callbackSuccessUrl) {
		this.callbackSuccessUrl = callbackSuccessUrl;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Map<String, String> getMetadata() {
		return metadata;
	}
	public void setMetadata(Map<String, String> metadata) {
		this.metadata = metadata;
	}
	public Long getValidity() {
		return validity;
	}
	public void setValidity(Long validity) {
		this.validity = validity;
	}

	
	
}
