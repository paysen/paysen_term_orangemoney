package sn.solution.paysen.entities;

import java.io.Serializable;

public class RequestCORestlet implements Serializable{

	private Long amount;
	private String currency;
	private String customermsisdn;
	//private String agentmsisdn;
	private String password;
	private String username;
	private String externalid;
	private String url_cashout;
	
	
	public Long getAmount() {
		return amount;
	}
	public void setAmount(Long amount) {
		this.amount = amount;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getCustomermsisdn() {
		return customermsisdn;
	}
	public void setCustomermsisdn(String customermsisdn) {
		this.customermsisdn = customermsisdn;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getExternalid() {
		return externalid;
	}
	public void setExternalid(String externalid) {
		this.externalid = externalid;
	}
	public String getUrl_cashout() {
		return url_cashout;
	}
	public void setUrl_cashout(String url_cashout) {
		this.url_cashout = url_cashout;
	}
	
}
