package sn.solution.paysen.entities;

import java.io.Serializable;

public class RequestCIRestlet implements Serializable{

	private PartnerObject partner;
	private CustomerObject customer;
	private AmountObject amount;
	private String reference;
	private Boolean receiveNotification;

	
	public PartnerObject getPartner() {
		return partner;
	}
	public void setPartner(PartnerObject partner) {
		this.partner = partner;
	}
	public CustomerObject getCustomer() {
		return customer;
	}
	public void setCustomer(CustomerObject customer) {
		this.customer = customer;
	}
	public AmountObject getAmount() {
		return amount;
	}
	public void setAmount(AmountObject amount) {
		this.amount = amount;
	}
	public String getReference() {
		return reference;
	}
	public void setReference(String reference) {
		this.reference = reference;
	}
	public Boolean getReceiveNotification() {
		return receiveNotification;
	}
	public void setReceiveNotification(Boolean receiveNotification) {
		this.receiveNotification = receiveNotification;
	}
	
	
	
}
