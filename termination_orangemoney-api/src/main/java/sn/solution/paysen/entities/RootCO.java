package sn.solution.paysen.entities;

import java.io.Serializable;

public class RootCO implements Serializable{

	private String status;
	private String message;
	private Double amount;
	private String currency;
	private Long customermsisdn;
	private Long externalid;
	private String approvalexpirytime;
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public Long getCustomermsisdn() {
		return customermsisdn;
	}
	public void setCustomermsisdn(Long customermsisdn) {
		this.customermsisdn = customermsisdn;
	}
	public Long getExternalid() {
		return externalid;
	}
	public void setExternalid(Long externalid) {
		this.externalid = externalid;
	}
	public String getApprovalexpirytime() {
		return approvalexpirytime;
	}
	public void setApprovalexpirytime(String approvalexpirytime) {
		this.approvalexpirytime = approvalexpirytime;
	}
	
	
}
