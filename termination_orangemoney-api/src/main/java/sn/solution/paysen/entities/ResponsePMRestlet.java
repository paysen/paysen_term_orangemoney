package sn.solution.paysen.entities;

import java.io.Serializable;
import java.util.Map;

public class ResponsePMRestlet implements Serializable{

	private Map<String, String> deepLinks;
	private String qrCode;
	private Long validity;
	private Map<String, String> metadata;
	private String qrId;
	private ValidorObject validFor;
	private String status;
	private String type;
	private String detail;
	private String code;
	private String errorCode;
	private String errorMessage;
	
	
	public Map<String, String> getDeepLinks() {
		return deepLinks;
	}
	public void setDeepLinks(Map<String, String> deepLinks) {
		this.deepLinks = deepLinks;
	}
	public String getQrCode() {
		return qrCode;
	}
	public void setQrCode(String qrCode) {
		this.qrCode = qrCode;
	}
	public Long getValidity() {
		return validity;
	}
	public void setValidity(Long validity) {
		this.validity = validity;
	}
	public String getQrId() {
		return qrId;
	}
	public void setQrId(String qrId) {
		this.qrId = qrId;
	}
	public ValidorObject getValidFor() {
		return validFor;
	}
	public void setValidFor(ValidorObject validFor) {
		this.validFor = validFor;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getDetail() {
		return detail;
	}
	public void setDetail(String detail) {
		this.detail = detail;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public Map<String, String> getMetadata() {
		return metadata;
	}
	public void setMetadata(Map<String, String> metadata) {
		this.metadata = metadata;
	}
	

	
	
}
