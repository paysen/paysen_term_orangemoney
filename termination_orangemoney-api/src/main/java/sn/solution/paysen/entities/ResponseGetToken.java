package sn.solution.paysen.entities;

import java.io.Serializable;

public class ResponseGetToken implements Serializable{

	private String access_token;
	private String token_type;
	private Long expires_in;
	private String scope;
	private String refresh_token;
	private Long refresh_expires_in;
	private String errorCode;
	private String errorMessage;
	
	public String getAccess_token() {
		return access_token;
	}
	public void setAccess_token(String access_token) {
		this.access_token = access_token;
	}
	public String getToken_type() {
		return token_type;
	}
	public void setToken_type(String token_type) {
		this.token_type = token_type;
	}
	public Long getExpires_in() {
		return expires_in;
	}
	public void setExpires_in(Long expires_in) {
		this.expires_in = expires_in;
	}
	public String getScope() {
		return scope;
	}
	public void setScope(String scope) {
		this.scope = scope;
	}
	public String getRefresh_token() {
		return refresh_token;
	}
	public void setRefresh_token(String refresh_token) {
		this.refresh_token = refresh_token;
	}
	public Long getRefresh_expires_in() {
		return refresh_expires_in;
	}
	public void setRefresh_expires_in(Long refresh_expires_in) {
		this.refresh_expires_in = refresh_expires_in;
	}
	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	
	
}
