package sn.solution.paysen.entities;

import java.io.Serializable;

public class PartnerObject implements Serializable{

	private String idType;
	private String id;
	private String encryptedPinCode;
	public String getIdType() {
		return idType;
	}
	public void setIdType(String idType) {
		this.idType = idType;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getEncryptedPinCode() {
		return encryptedPinCode;
	}
	public void setEncryptedPinCode(String encryptedPinCode) {
		this.encryptedPinCode = encryptedPinCode;
	}
	
	
}
