package sn.solution.paysen.entities;

import java.io.Serializable;

public class ResponseCORestlet implements Serializable{

	private String status;
	private String message;
	private Long amount;
	private String currency;
	private String customermsisdn;
	private String externalid;
	private String approvalexpirytime;
	private RootCO root;
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Long getAmount() {
		return amount;
	}
	public void setAmount(Long amount) {
		this.amount = amount;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getCustomermsisdn() {
		return customermsisdn;
	}
	public void setCustomermsisdn(String customermsisdn) {
		this.customermsisdn = customermsisdn;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getExternalid() {
		return externalid;
	}
	public void setExternalid(String externalid) {
		this.externalid = externalid;
	}
	public String getApprovalexpirytime() {
		return approvalexpirytime;
	}
	public void setApprovalexpirytime(String approvalexpirytime) {
		this.approvalexpirytime = approvalexpirytime;
	}
	public RootCO getRoot() {
		return root;
	}
	public void setRoot(RootCO root) {
		this.root = root;
	}
	
	
	
}
