package sn.solution.paysen.entities;

import java.io.Serializable;
import java.util.Map;

public class TransactionModel implements Serializable{

	private String errorCode;
	private String errorMessage;
	private String codeService;
	private Double montant;
	private String numeroBeneficiaire;
	private String url_getToken;
	private String url_getPublicKey;
	private String url_cashin;
	private String url_callBack;
	private String prenomBeneficiaire;
	private String nomBeneficiaire;
	private String mailBeneficiaire;
	private String identifiantTXPaysen;	
	private String client_id;
	private String client_secret;
	private String grant_type;
	private String msisdn;
	private String numeroDistributeur;
	private String unit_amount;
	private String refTransaction;
	private String access_tokenOM;
	private String pin;
	private String public_key;
	private String callbackCancelUrl;
	private String callbackSuccessUrl;
	private String code;
	private String name;
	private Long validity;
	private String url_payment;
	private String callbackEndTx;
	private Map<String, String> deepLinks;
	private String qrCode;
	private Map<String, String> metadata;
	private String qrId;
	private ValidorObject validFor;
	private String status;
	private String type;
	private String detail;

	
	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public String getCodeService() {
		return codeService;
	}
	public void setCodeService(String codeService) {
		this.codeService = codeService;
	}
	public Double getMontant() {
		return montant;
	}
	public void setMontant(Double montant) {
		this.montant = montant;
	}
	public String getNumeroBeneficiaire() {
		return numeroBeneficiaire;
	}
	public void setNumeroBeneficiaire(String numeroBeneficiaire) {
		this.numeroBeneficiaire = numeroBeneficiaire;
	}

	public String getPrenomBeneficiaire() {
		return prenomBeneficiaire;
	}
	public void setPrenomBeneficiaire(String prenomBeneficiaire) {
		this.prenomBeneficiaire = prenomBeneficiaire;
	}
	public String getNomBeneficiaire() {
		return nomBeneficiaire;
	}
	public void setNomBeneficiaire(String nomBeneficiaire) {
		this.nomBeneficiaire = nomBeneficiaire;
	}
	public String getMailBeneficiaire() {
		return mailBeneficiaire;
	}
	public void setMailBeneficiaire(String mailBeneficiaire) {
		this.mailBeneficiaire = mailBeneficiaire;
	}
	public String getIdentifiantTXPaysen() {
		return identifiantTXPaysen;
	}
	public void setIdentifiantTXPaysen(String identifiantTXPaysen) {
		this.identifiantTXPaysen = identifiantTXPaysen;
	}
	public String getUrl_getToken() {
		return url_getToken;
	}
	public void setUrl_getToken(String url_getToken) {
		this.url_getToken = url_getToken;
	}
	public String getUrl_getPublicKey() {
		return url_getPublicKey;
	}
	public void setUrl_getPublicKey(String url_getPublicKey) {
		this.url_getPublicKey = url_getPublicKey;
	}
	public String getUrl_cashin() {
		return url_cashin;
	}
	public void setUrl_cashin(String url_cashin) {
		this.url_cashin = url_cashin;
	}
	public String getUrl_callBack() {
		return url_callBack;
	}
	public void setUrl_callBack(String url_callBack) {
		this.url_callBack = url_callBack;
	}
	public String getClient_id() {
		return client_id;
	}
	public void setClient_id(String client_id) {
		this.client_id = client_id;
	}
	public String getClient_secret() {
		return client_secret;
	}
	public void setClient_secret(String client_secret) {
		this.client_secret = client_secret;
	}
	public String getGrant_type() {
		return grant_type;
	}
	public void setGrant_type(String grant_type) {
		this.grant_type = grant_type;
	}
	public String getMsisdn() {
		return msisdn;
	}
	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}
	public String getNumeroDistributeur() {
		return numeroDistributeur;
	}
	public void setNumeroDistributeur(String numeroDistributeur) {
		this.numeroDistributeur = numeroDistributeur;
	}
	public String getUnit_amount() {
		return unit_amount;
	}
	public void setUnit_amount(String unit_amount) {
		this.unit_amount = unit_amount;
	}
	public String getRefTransaction() {
		return refTransaction;
	}
	public void setRefTransaction(String refTransaction) {
		this.refTransaction = refTransaction;
	}
	public String getAccess_tokenOM() {
		return access_tokenOM;
	}
	public void setAccess_tokenOM(String access_tokenOM) {
		this.access_tokenOM = access_tokenOM;
	}
	public String getPin() {
		return pin;
	}
	public void setPin(String pin) {
		this.pin = pin;
	}
	public String getPublic_key() {
		return public_key;
	}
	public void setPublic_key(String public_key) {
		this.public_key = public_key;
	}
	public String getCallbackCancelUrl() {
		return callbackCancelUrl;
	}
	public void setCallbackCancelUrl(String callbackCancelUrl) {
		this.callbackCancelUrl = callbackCancelUrl;
	}
	public String getCallbackSuccessUrl() {
		return callbackSuccessUrl;
	}
	public void setCallbackSuccessUrl(String callbackSuccessUrl) {
		this.callbackSuccessUrl = callbackSuccessUrl;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Long getValidity() {
		return validity;
	}
	public void setValidity(Long validity) {
		this.validity = validity;
	}
	public String getUrl_payment() {
		return url_payment;
	}
	public void setUrl_payment(String url_payment) {
		this.url_payment = url_payment;
	}
	public String getCallbackEndTx() {
		return callbackEndTx;
	}
	public void setCallbackEndTx(String callbackEndTx) {
		this.callbackEndTx = callbackEndTx;
	}
	public Map<String, String> getDeepLinks() {
		return deepLinks;
	}
	public void setDeepLinks(Map<String, String> deepLinks) {
		this.deepLinks = deepLinks;
	}
	public String getQrCode() {
		return qrCode;
	}
	public void setQrCode(String qrCode) {
		this.qrCode = qrCode;
	}
	public Map<String, String> getMetadata() {
		return metadata;
	}
	public void setMetadata(Map<String, String> metadata) {
		this.metadata = metadata;
	}
	public String getQrId() {
		return qrId;
	}
	public void setQrId(String qrId) {
		this.qrId = qrId;
	}
	public ValidorObject getValidFor() {
		return validFor;
	}
	public void setValidFor(ValidorObject validFor) {
		this.validFor = validFor;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getDetail() {
		return detail;
	}
	public void setDetail(String detail) {
		this.detail = detail;
	}
	
	
}
