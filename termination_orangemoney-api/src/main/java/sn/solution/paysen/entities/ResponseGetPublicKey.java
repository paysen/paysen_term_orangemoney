package sn.solution.paysen.entities;

import java.io.Serializable;

public class ResponseGetPublicKey implements Serializable{

	private String keyId;
	private String keyType;
	private Long keySize;
	private String key;
	private String errorCode;
	private String errorMessage;
	
	public String getKeyId() {
		return keyId;
	}
	public void setKeyId(String keyId) {
		this.keyId = keyId;
	}
	public String getKeyType() {
		return keyType;
	}
	public void setKeyType(String keyType) {
		this.keyType = keyType;
	}
	public Long getKeySize() {
		return keySize;
	}
	public void setKeySize(Long keySize) {
		this.keySize = keySize;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	
	
}
